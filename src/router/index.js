import Vue from 'vue'
import Router from 'vue-router'

import RecipeIndex from '../components/recipesModule/index'
import RecipeShow from '../components/recipesModule/show'
import RecipeNew from '../components/recipesModule/new'
import RecipeUpdate from '../components/recipesModule/update'

import SpecialIndex from '../components/specialsModule/index'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    { path: '/recipes',
      name: 'Recipes',
      component: RecipeIndex
    },
    { path: '/recipes/new',
      name: 'RecipeNew',
      component: RecipeNew
    },
    { path: '/recipes/show/:id',
      name: 'RecipeShow',
      component: RecipeShow
    },
    { path: '/recipes/update/:id',
      name: 'RecipeUpdate',
      component: RecipeUpdate
    },
    { path: '/specials',
      name: 'Specials',
      component: SpecialIndex
    },
    { path: '*',
      component: RecipeIndex
    }
  ]
})
